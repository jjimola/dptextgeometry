# Experiments and Code for Jacob's Intern Project

## Documentation
## Installing Prerequisite Libraries

This codebase uses the standard data scince libraries NumPy, SciPy, and Matplotlib.
Also, the codebase makes use of 
the [Annoy](https://github.com/spotify/annoy) Nearest neighbor python package
and [TQDM](https://github.com/tqdm/tqdm), a progress bar library. These packages can be
installed via Pip:
```bash
pip install annoy
pip install tqdm
```

## Word Embedding and Embedding Generation

We represent a word embedding in the Embeddings class in embeddings.py. This
class contains the words and word vectors contained in a word embedding. An
important function in this file, called load_embeddings, will create an
Embeddings class from a file containing word, vector attributes in
space-separated format. We might use the following code to do this, assuming
the embeddings file is called glove.6B.100d.txt:

```
from embeddings import load_embeddings
#Loads the first 200 words from the Glove embedding
e = load_embeddings('glove.6B.100d.txt', 200)
```

For the purposes of our experiment, we don't want to load entire word
embeddings as they are too large. Instead, we want to produce a representative sample of the
embedding. The Subsampler class of embeddings.py produces a subsampled word
embedding from a larger embedding where words are subsampled according to
proximity. The following code shows how to use a Subsampler to produce an
embedding.

```
from embeddings import Subsampler
ssmp = Subsampler('glove.6B.100d.txt')
save_file = 'data/glove_sample_1000_0.5.txt'
#Samples an embedding of size 500 from the Glove embedding.
#0.5 and 15 are parameters of the subsampling method.
ssmp.subsample_embedding(save_file, 500, 0.5, 15)
```

The file gen_embedding.py contains the code we used to produce the subsamples
used in the experiments.

## The Critical Components: The Mechanism Classes
The mechanisms are implemented in the file mechanism_base.py. Each mechanism
has its own class --- currently implemented is EuclideanMechanism (madlib),
ExponentialMechanism, OPTMech, and ConstrOPTMech. The constructor mechanism takes epsilon
and the word embedding as arguments. Euclidean and ConstrOPTMech take
additional arguments, see the code for full details. For example, this is how
we would construct a EuclideanMechanism class
```
from mechanism_base import EuclideanMechanism
directory = 'data/glove_sample_1000_0.5.txt'
#e is our embedding, we load 60 words from the sample
e = load_embeddings(directory, 60)
euclid_mech = EuclideanMechanism(1.0, 100, e)
```

Each mechanism class implements four important methods:

#### get_transition_matrix()
This computes the transition matrix of a mechanism, define as the matrix whose
i,j entry is the probability that the mechanism maps word i of the embedding to
word j of the embedding. This method saves the transition matrix in an
attribute called transition_matrix, and it must be called before virtually any of the
other methods are called since other methods use the transition_matrix
attribute. This is also often a time-intensive method depending on vocabulary
size.

#### get_private_word(word)
This runs the mechanism once from an input word. In other words, it gets the
private replacement word from the non-private input word

#### get_loss()
This finds the loss of the mechanism from its transition matrix. See our paper
for a definition of mechanism loss.

#### get_actual_epsilon()
This computes the smallest value of epsilon such that the mechanism satisfies
epsilon metric DP. It is the strongest level of privacy a mechanism provides.

We might collect privacy and loss of the Euclidean mechanism with the following:
```
from mechanism_base import EuclideanMechanism
#Code to initialize embedding e
epsilon = 1.0
euclid_mech = EuclideanMechanism(epsilon, 100, e)
euclid_mech.get_transition_matrix()
actual_epsilon = euclid_mech.get_actual_epsilon()
loss = euclid_mech.get_loss()
```

## Running Experiments and Jupyter Notebook

The file run_experiments_new.py implements all the experiments used in the final
paper. It uses the components outlined above. The three Jupyter notebooks
loads the result files and plots them, producing the plots
in our final paper.
