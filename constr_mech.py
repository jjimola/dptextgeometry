from embeddings import get_spanner
import numpy as np
from scipy.optimize import linprog
import pickle
import pdb

from mechanism_base import MechanismBase

class ExpConstrOPTMech(MechanismBase):

    """ Creates a Heuristic mechanism class. This class is identical to the LP
        mechanism class, except it fixes some of the P_{ij} variables to be
        fixed at Y_{j} \exp(-D_{ij} \epsilon), where Y_j are new variables.
        The class decides when do do this using its parameter I, which is an n
        by n matrix such that I[i,j] = 0 if it is to be fixed, and 1 otherwise.
        Parameters:
            epsilon: privacy parameter used by this class.
            embeddings: word embeddings instance (see embeddings.py).
            lambdas: amount of slack to use for constraint that transition matrix
                rows sum to 1. If a list, then the best choice of lambda will
                be used.
    """
    def __init__(self, epsilon, embeddings, max_exp = np.inf, alpha = 2.0,
            max_memory = None, spanner_coef = None):
        super().__init__(epsilon, embeddings)
        self.name="ConstrOPT"
        self.I = np.ones_like(embeddings.distance_matrix, dtype='int')
        self.solver_method = 'highs-ipm'
        self.solver_options = {}
        self.max_exp = max_exp
        self.alpha = alpha
        if max_memory is None:
            self.max_lpsize = np.inf
        else:
            self.max_lpsize = max_memory // 8

        self.spanner_coef = spanner_coef
    
    """ Sets the variables to not be fixed (self.I) to be those P_{ij} where i is
        not one of the r nearest neighbors to j.
        Paramters:
        num_nns: number of nearest neighbors to use (variables which are
            not one of the num_nns nearest points are fixed).
    """
    def set_nns_heuristic(self, nns):
        I = np.zeros_like(self.embeddings.distance_matrix, dtype=int)
        for i in range(0, I.shape[0]):
            closest_nbrs = \
                np.argsort(self.embeddings.distance_matrix[i,:])[:nns]
            I[i, closest_nbrs] = 1
        self.I = I

    """ Sets the variables to not be fixed (self.I) to be those P_{ij} where i is 
        one of r-1 randomly chosen words.
    """
    def set_random_heuristic(self, npts):
        n = self.embeddings.distance_matrix.shape[0]
        I = np.zeros_like(self.embeddings.distance_matrix, dtype=int)
        for i in range(0, I.shape[0]):
            random_nbrs = np.random.choice(n, npts, replace=False)
            I[random_nbrs, i] = 1
        self.I = I

    def get_transition_matrix(self):
        D = self.embeddings.distance_matrix
        I = self.I
        epsilon = self.epsilon/2
        n = D.shape[0]
        exp_mech = np.exp(-D * epsilon)
        row_sums = exp_mech.sum(axis=1)
        row_sums = row_sums.reshape((row_sums.size, -1))
        exp_mech /= row_sums
        self.transition_matrix = exp_mech
        epsilon = self.get_actual_epsilon()
        #print(epsilon)
        self.transition_matrix = None
        #D_exp = np.minimum(self.max_exp / epsilon, D)
        idxs = -np.ones((n,n), dtype=int)
        count = 0
        for i in range(0, n):
            for j in range(0, n):
                if I[j,i] != 0:
                    idxs[j,i] = count
                    count += 1

        C = []
        num_vars = count+1
        lb = np.zeros(num_vars)
        ub = np.ones(num_vars)
        lb[-1] = 0
        ub[-1] = None
        for i in range(0, n):
            active_vars = np.where(I[:,i].flatten() != 0)[0]
            if self.spanner_coef is None:
                include = np.ones((n, n), dtype=int)
            else:
                point_dists = D[active_vars][:, active_vars]
                include, _ = get_spanner(point_dists, self.spanner_coef)
            for a in range(0, len(active_vars)):
                for b in range(0, len(active_vars)):
                    if a != b and include[a,b]:
                        j = active_vars[a]
                        k = active_vars[b]
                        j_idx = idxs[j,i]
                        k_idx = idxs[k,i]
                        row = np.zeros(num_vars)
                        #P_{ji} \leq \exp(D_{jk} * eps) P_{ki}
                        row[j_idx] = 1
                        row[k_idx] = -np.exp(D[k, j] * epsilon)
                        C.append(row)
                        if len(C) * len(C[0]) > self.max_lpsize:
                            print("LP too large. Aborting")
                            return None

            non_active_vars = I[:,i] == 0
            if np.any(non_active_vars):
                for j in active_vars:
                    j_idx = idxs[j, i]
                    #P_{ji} \leq min over k: \exp(D_{jk} * eps) exp_mech_{ki}
                    #P_{ji} \geq max over k: \exp(-D_{jk} * eps) exp_mech_{ki}
                    coeff_up = np.exp(D[j, non_active_vars] * epsilon) * \
                        exp_mech[non_active_vars, i]
                    coeff_up = coeff_up.min()
                    coeff_down = np.exp(-D[j, non_active_vars] * epsilon)* \
                        exp_mech[non_active_vars, i]
                    coeff_down = coeff_down.max()
                    lb[j_idx] = coeff_down
                    ub[j_idx] = coeff_up
                    assert coeff_down < coeff_up

        for i in range(0, n):
            row = np.zeros(num_vars)

            active_vars = np.where(I[i,:] != 0)[0]
            active_vars_idxs = idxs[i, active_vars]
            row[active_vars_idxs] = D[i, active_vars]
            row[-1] = -1
            C.append(row)

        b = np.zeros(len(C))

        c = []
        for i in range(0, n):
            row = np.zeros(num_vars)
            active_vars = np.where(I[i,:] != 0)[0]
            active_vars_idxs = idxs[i, active_vars]
            row[active_vars_idxs] = 1
            mass = exp_mech[i, active_vars].sum()
            C.append(row)
            c.append(mass * self.alpha)
            C.append(-row)
            c.append(-mass)
        
        C = np.array(C)
        self.memory = C.size
        b = np.concatenate((b, np.array(c)))
        obj = np.zeros(num_vars)
        obj[-1] = 1
        
        L = linprog(obj, C, b, method=self.solver_method, \
            bounds = (0, None), options = self.solver_options)
        if not L['success']:
            print("LP not successful!")
            print("Message from Solver: %s" % L['message'])
            if L['x'] is None:
                return None
        sol = L['x']
        ans = exp_mech
        row_coefs = sol[0:n]
        for i in range(0, n):
            active_vars = np.where(I[i,:] != 0)[0]
            ans[i, active_vars] = sol[idxs[i, active_vars]]
        
        row_sums = ans.sum(axis=1)
        row_sums = row_sums.reshape((row_sums.size, -1))
        ans /= row_sums
        self.transition_matrix = ans
        return ans

