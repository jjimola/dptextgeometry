import numpy as np
import reader
from annoy import AnnoyIndex
from tqdm import tqdm

from typing import Dict, List, Tuple, Any
from numpy import ndarray

Embedding = ndarray

#TODO: use a sparse graph.
def get_spanner(D, k):
    n = D.shape[0]
    dists = np.full((n,n), np.inf)
    graph = np.zeros((n,n))
    dists.fill(np.inf)
    for i in range(0, n):
        dists[i,i] = 0

    idxs = D.flatten().argsort()
    for c in idxs:
        i = c // n
        j = c % n
        if i <= j:
            continue
        if (2*k-1) * D[i,j] < dists[i,j]:
            graph[i,j] = 1
            graph[j,i] = 1
            i_to_k = dists[i,:].copy()
            j_to_l = dists[j,:].copy()
            for l in range(0, n):
                dists[l, :] = np.minimum(dists[l, :], i_to_k + j_to_l[l] + D[i,j])
                dists[:, l] = dists[l, :]
                """
                for kp in range(0, n):
                    dists[l, kp] = min(dists[l,kp], i_to_k[kp] + D[i,j] +
                            j_to_l[l])
                    dists[kp, l] = dists[l, kp]
                """
    return (graph, dists)


class Embeddings:
    "Word embeddings and related metadata."

    # FIXME: uncomment when python > 3.5 supported
    # vocab_list = [] : List[str]
    # vocab_dict = {} : Dict[str, int]
    # embds_dict = {} : Dict[str, ndarray]
    # embeddings = [] : List[ndarray]
    # path = '' : str

    def __init__(
        self,
        vocab_list: List[str],
        vocab_dict: Dict[str, int],
        embds_dict: Dict[str, ndarray],
        embeddings: List[ndarray],
        path: str
    ) -> None:
        """
        :param vocab_list: ordered list of words
        :param vocab_dict: map from word to vector index
        :param embds_dict: map of words to vector
        :param embeddings: list of all word vectors ordered by vocab_list
        :param path: filepath to embeddings file
        """
        self.vocab_list = vocab_list
        self.vocab_dict = vocab_dict
        self.embds_dict = embds_dict
        self.embeddings = embeddings
        self.path = path

        idx = len(path)-1
        while path[idx] != '.':
            idx -= 1
        self.ann_path = path[:idx] + '.ann'

    def create_index(self, n):
        """
        Create approximate nearest neighbor index
        :param embeddings: [embedding]
        :param n: number of indexes to build
        :return: annoy index
        """
        t = AnnoyIndex(len(self.embeddings[0]), 'euclidean')
        for i, v in enumerate(self.embeddings):
            t.add_item(i, v)
        t.build(n)

        self.annoy_index = t

    def compute_distance_matrix(self):
        """Computes distance matrix, or the matrix whose i,j entry is the
            distance between word w_i and w_j of the embedding.
        """
        num_words = len(self.vocab_list)
        dist_mat = np.zeros((num_words, num_words))
        for i in range(0, num_words):
            for j in range(i+1, num_words):
                v1 = self.embeddings[i]
                v2 = self.embeddings[j]
                D = np.linalg.norm(v1-v2)
                dist_mat[i][j] = D
                dist_mat[j][i] = D
        self.distance_matrix = dist_mat

    def get_spanner(self, k):
        return get_spanner(self.distance_matrix, k)

    def save_ann(self):
        """Saves the annoy instance to a file since it can't be pickled. This
        is important for running parallel code"""
        self.annoy_index.save(self.ann_path)
        self.annoy_index = None

    def load_ann(self):
        """Loads the annoy instance from a file since it can't be pickled. This
        is important for running parallel code"""
        self.annoy_index = AnnoyIndex(len(self.embeddings[0]), 'euclidean')
        self.annoy_index.load(self.ann_path)
        

def load_embeddings(emb_path: str, limit_nb_emb: int) -> Embeddings:
    """
    Creates an Embedding class from a file that contains an embedding
    embedding
    
    Parameters:
        emb_path: path to embedding file
        limit_nb_emb: Maximum of words to read from embedding file before stopping.

    """
    emb_dict = reader.load_embeddings_as_dict(emb_path, limit_nb_emb)
    vocab_list, vocab_dict = build_vocabulary(emb_dict, limit_nb_emb)
    emb_list = [np.array(emb_dict[w]) for w in vocab_list]
    _emb_dict = {w: np.array(emb_dict[w]) for w in vocab_list}

    e = Embeddings(vocab_list, vocab_dict, _emb_dict, emb_list, emb_path)
    e.create_index(15)
    e.compute_distance_matrix()
    return e

def build_vocabulary(
    embds_dict: Dict[str, ndarray], limit_nb_emb: int
) -> Tuple[List[str], Dict[Any, Any]]:
    """
    Builds vocabulary-related arrays from a word-to-vector dictionary. These
    arrays are a list containing the words in the vocabulary and an dictionary
    from vocabulary to index in the list

    Parameters:
        embds_dics: word-to-vector dictionary
        limit_nb_emb: maximum number of words in dictionary to read before
        stopping.
    """
    words = list(embds_dict.keys())

    vocab_sets = words[:limit_nb_emb]
    vocab_list = np.array(list(vocab_sets))
    vocab_dict = {w: i for i, w in enumerate(vocab_list)}

    return vocab_list, vocab_dict

class Subsampler:
    """
    A class that produces subsamples from a given word embedding. The
    subsamples are not uniformly sampled, but sampled with higher probabilities
    given to a samples already produced. 

    Parameters:
        emb_path_in: path to embedding file
        limit_nb_emb: Number of words to graph from this file.
    """
    def __init__(self, emb_path_in, limit_nb_emb):
        emb_dict = reader.load_embeddings_as_dict(emb_path_in, limit_nb_emb)
        words = list(emb_dict.keys())
        t = AnnoyIndex(len(emb_dict[words[0]]), 'euclidean')
        for i in range(0, len(words)):
            t.add_item(i, emb_dict[words[i]])

        t.build(50)
        self.emb_dict = emb_dict
        self.words = words
        self.t = t
       
    """
    Produces a sampled embedding from embedding of the class and saves it to a
    file.

    Parameters:
        emb_path_out: path to subsampled embedding file (to be written to).
        num_select: number of words in the subsample
        p_cluster: With this probability, pick a random word already taken and
            add a random nearest neighbors to the sample.
            Otherwise, add a random word to the sample
        num_nns: The number of nearest neighbors around a point to choose from.

    """
    def subsample_embedding(self, emb_path_out, num_select, p_cluster, num_nns):
        emb_dict = self.emb_dict
        words = self.words
        
        idxs = [np.random.choice(len(words))]
        for i in range(num_select):
            if np.random.uniform() < p_cluster:
                idx = np.random.choice(idxs)
                nbrs = self.t.get_nns_by_item(idx, num_nns)
                idxs.append(np.random.choice(nbrs))
            else:
                idxs.append(np.random.choice(len(words)))
        
        #Remove duplicates
        idxs = set(idxs)
        f = open(emb_path_out, 'w')
        for i in idxs:
            word = words[i]
            line = word + ' ' + ' '.join(map(str, emb_dict[word])) + '\n'
            f.write(line) 
        f.close()

class TestEmbeddings:
    def __init__(self, emb_path):
        self.emb_path = emb_path

    def test_spanner(self, limit_nb_words, k):
        emb = load_embeddings(self.emb_path, limit_nb_words)
        return (*emb.get_spanner(k), emb)
