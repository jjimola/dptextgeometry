from embeddings import Subsampler
ssmp = Subsampler('../glove.6B.300d.txt', None)
for num_samp in [1000]:
    for p in [0.3, 0.5, 0.8]:
        f_name = 'data/glove_sample_' + str(num_samp) + '_' + str(p) + '.txt'
        ssmp.subsample_embedding(f_name, num_samp, p, 15)
"""

ssmp = Subsampler('../wiki-news-300d-1M.vec', None)
for num_samp in [1000]:
    for p in [0.3, 0.5, 0.8]:
        f_name = 'data/fasttext_sample_' + str(num_samp) + '_' + str(p) + '.txt'
        ssmp.subsample_embedding(f_name, num_samp, p, 15)

"""
