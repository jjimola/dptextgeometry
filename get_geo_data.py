import pandas as pd

import os

def get_traj_data(fname, max_loc_per_user):
    df = pd.read_csv(fname, header=None, skiprows=6)[[0,1]]
    df = (df / 0.005).astype(int)
    hist = df.value_counts()
    hist = hist.iloc[0:min(max_loc_per_user, hist.size)]
    return list(hist.index)

counts = dict()
directory = '../Geolife Trajectories 1.3/Data'
for user in os.listdir(directory):
    user_directory = os.path.join(directory, user, 'Trajectory')
    for traj in os.listdir(user_directory):
        f = os.path.join(user_directory, traj)
        locs = get_traj_data(f, 30)
        for l in locs:
            if l in counts:
                counts[l] += 1
            else:
                counts[l] = 1

sorted_locs = sorted(counts, key=lambda x:-counts[x])
if len(sorted_locs) > 1000:
    sorted_locs = sorted_locs[:1000]

g = open('data/geolife.txt', 'w')
for i in range(0, len(sorted_locs)):
    g.write('%d %d %d' % (i, *sorted_locs[i]))
