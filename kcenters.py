import numpy as np

"""
This class maintains a k-center clustering of the points whose distances are
contained in the matrix D. It does this with the global variables

center_idxs: center_idxs[i] contains the cluster representative of point i.
centers: set of centers in 

"""
class Kcenters:
    """ 
    Initialization method.
    Parameters:
        D - distance matrix
        
    """
    def __init__(self, D):
        self.D = D
        self.center_idxs = None
        self.centers = None

    """
    K-centers seeding method. This greedy algorithm takes the first available
    center and all those of distance less than rad to it.
    Parameters:
        rad - Max radius of clusterings.
    """

    def kcenters_seed(self, rad):
        center_idxs = -np.ones(len(self.D), dtype=int)
        centers = []
        for i in range(0, len(self.D)):
            if center_idxs[i] == -1:
                centers.append(i)
                untaken = center_idxs == -1
                centers_to_take = (self.D[i,:] < rad) & untaken
                center_idxs[centers_to_take] = i
        self.center_idxs = center_idxs
        self.centers = centers

    """
    Performs a round of cluster improvement: If a word w is closter to a center
    c than the center assigned in center_idxs[w], then we set center_idxs[w] to
    c.

    """
    def update_clusters(self):
        for i in range(0, len(self.center_idxs)):
            self.center_idxs[i] = self.centers[np.argmin(self.D[i, self.centers])]
    """
    Performs a round of center improvement: If a cluster can be better
    represented by a different center (i.e. its radius goes down), then we
    update it.

    """
    def update_centers(self):
        for i in range(0, len(self.centers)):
            cluster = self.center_idxs == self.centers[i]
            D_max = self.D[cluster][:, cluster].max(axis=1)
            best_center_idx = np.argmin(D_max)
            best_center = np.where(cluster)[0][best_center_idx]
            self.center_idxs[cluster] = best_center
            self.centers[i] = best_center

    """ 
    Computes k center cost of current clustering
    """

    def get_kcenter_cost(self):
        cost = 0
        for c in self.centers:
            cluster = self.center_idxs == c
            cluster_cost = self.D[c, cluster].max()
            cost = max(cluster_cost, cost)
        return cost

    """
    Trains a kcenters clustering by iteratively updating centers and clusters according
    to the kcenters cost function. Does this until max_iterations is reached or 
    until there is no reduction in the kcenter cost.
    """
    def train_kcenters(self, max_iterations = 500):
        C = self.get_kcenter_cost()
        for i in range(0, max_iterations):
            self.update_clusters()
            self.update_centers()
            C_new = self.get_kcenter_cost()
            if C == C_new:
                break
            C_new = C
        
