import numpy as np
from kcenters import Kcenters

"""
This method computes the lower bound on loss given a list of epsilons.
It searches over a set of packings defined by the output of different calls to
KCenters with an increasing greedy radius for the seeding part of Kcenters. 
It then computes the best lower bound for each input value of epsilon. This 
is not necessarily the best possible lower bound, but produces strong candidates
because it forces the clusters in the packing to be far apart from each other 
by using KCenters.

parameters:
    D: distance matrix
    eps_list: list of epsilons for which to compute lower bound    


"""
def lower_bound(D, eps_list):
    highest_losses = np.zeros(len(eps_list))
    rad_seed = D[D > 0].min()
    max_rad = D.max()
    while rad_seed <= max_rad:
        kc = Kcenters(D)
        kc.kcenters_seed(2*rad_seed)
        kc.train_kcenters()
        rad = kc.get_kcenter_cost() / 2
        dist_centers = D[:,kc.centers]
        for i in range(len(eps_list)):
            eps = eps_list[i]
            #This computes the value N(w,S)
            NwS = np.exp(-dist_centers * eps).sum(axis=1)
            L = rad * (1 - 1/NwS).max()
            highest_losses[i] = max(highest_losses[i], L)
        rad_seed *= 1.1
    return highest_losses
