from embeddings import load_embeddings, get_spanner
import numpy as np
from scipy.sparse import lil_matrix
from scipy.optimize import linprog
import pickle
from kcenters import Kcenters
import pdb
import multiprocessing as mp

class MechanismBase:
    """ Creates a mechanism class. 
        Parameters:
            epsilon: privacy parameter used by this class.
            embeddings: word embeddings instance (see embeddings.py).
    """
    def __init__(self, epsilon, embeddings):
        self.epsilon = epsilon
        self.transition_matrix = None
        self.embeddings = embeddings
        self.name=None
    """ Returns the transition matrix. Implemented by inheriting class
    """
    def get_transition_matrix(self):
        pass

    """ Returns a noisy word from a given word.. Implemented by inheriting class
        Parameters: 
            word: Noise is added to this.
        
    """ 
    def get_private_word(self, word):
        pass

    """ Loads embeddings from a given file path.
        Parameters:
            emb_path: File path (A file containing word vectors"
            limit_nb_emb: Maximum number of words to load from the file
    """
    def load_embeddings(self, emb_path, limit_nb_emb):
        e = load_embeddings(emb_path, limit_nb_emb)
        self.embeddings = e
    
    """ Computes mechanism loss from the transition matrix
        Parameters: 
            typ: Type of loss to return. Takes one of three options:
                'mean' is the loss averaged over every word.
                'max' is the maximum loss over every word.
                'whole' is the entire loss vector.
    """
    def get_loss(self, typ='max'):
        if self.transition_matrix is None:
            print("Please compute transition matrix")
            return None
        word_errors = (self.embeddings.distance_matrix *
        self.transition_matrix).sum(axis=1)
        if typ=='mean':
            return word_errors.mean()
        elif typ == 'whole':
            return word_errors
        else:
            return word_errors.max()

    """ Computes the actual epsilon value from a transition matrix. This is
        defined as the minimum value of epsilon such that the entries of the
        transition matrix satisfy (epsilon, delta) metric DP. Normally, when
        computing epsilon, we consider the maximum ratio of probability
        distributions P and Q, where P,Q are the distributions of the mechanism
        with two different input words. Essentially
        (epsilon, delta) metric DP means that we can discard some words whose
        total mass on Q is no more than delta, and then compute the max of P/Q.
        We discard those words where P/Q is the highest and then compute
        epsilon from this.
        Parameters:
            delta: The value of delta (when set to 0.0, this is equivalent to
            epsilon metric DP).
    """
    def get_actual_epsilon(self, delta= 0.0):
        if self.transition_matrix is None:
            print("Please compute transition matrix")
            return None
        T = self.transition_matrix
        n = T.shape[1]
        eps_actual = 0.0
        for i in range(0, n):
            for j in range(0, n):
                if i == j:
                    continue
                D = self.embeddings.distance_matrix[i,j]
                P = T[i,:]
                Q = T[j,:]
                support = np.any((P > 0, Q > 0), axis=0)
                P = P[support]
                Q = Q[support]
                #Division by 0 has the proper behavior here.
                with np.errstate(divide='ignore'):
                    ratios = P / Q
                idxs = np.argsort(ratios)
                probs = Q[idxs].cumsum()
                #The last elements of probs must be 1.
                probs[probs == probs[-1]] = 1
                #cutoff is the smallest index where >= 1-delta of Q's mass is
                cutoff = np.searchsorted(probs, 1-delta, side='left')
                eps = np.log(ratios[idxs[cutoff]]) / D
                eps_actual = max(eps_actual, eps)
        return eps_actual
    """ Computes the vector of N_w values for each word w. N_w is defined as
        the probability that the mechanism sends a word w to itself.

    """
    def get_Nw_vec(self):
        T = self.transition_matrix
        if T is None:
            print("Please compute transition matrix")
            return None
        return T.diagonal()

    """ Computes the vector of S_w values for each word w. S_w is defined as
        the number of words s such that the probability the mechanism sends w
        to s is at least tau.
        Parameters:
            frac: the value tau in the definition above.
    """
    def get_Sw_vec(self, frac):
        T = self.transition_matrix
        if T is None:
            print("Please compute transition matrix")
            return None
        T = self.transition_matrix
        return (T >= frac).sum(axis=1)

    """ Save the transition matrix to a file.
        Paramters:
            prefix: folder in which to save the file.
    """
    def save_transition_matrix(self, prefix = 'matrices/'):
        S = self.embeddings.path
        idx = len(S)-1
        while S[idx] != '.':
            idx -= 1
        path = prefix + S[:idx] + '_' + self.name + '.pkl'
        pickle.dump(self.transition_matrix, open(path, 'wb'))

    """ Loads the transition matrix from a file.
        Parameters:
            prefix: folder in which to load the file.
    """
    def load_transition_matrix(self, prefix = 'matrices/'):
        S = self.embeddings.path
        idx = len(S)-1
        while S[idx] != '.':
            idx -= 1
        path = prefix + S[:idx] + '_' + self.name + '.pkl'
        self.transition_matrix = pickle.load(open(path, 'rb'))


class EuclideanMechanism(MechanismBase):
    """ Creates a Euclidean mechanism class. 
        Parameters:
            epsilon: privacy parameter used by this class.
            num_samples: number of samples to use to estimate the transition
                matrix.
            embeddings: word embeddings instance (see embeddings.py).
            compute_parallel: whether to compute in parallel
    """
    def __init__(self, epsilon, num_samples, embeddings, compute_parallel=False):
        super().__init__(epsilon, embeddings)
        self.name="Euclidean"
        self.num_samples = num_samples
        self.compute_parallel = compute_parallel
    
    """ Computes an approximation of the transition matrix. It simply calls
        get_private_word num_samples times for each word. For accurate results,
        I recommend setting num_samples >= 10 times the number of words.

    """
    def get_transition_matrix(self):
        embeddings = self.embeddings
        num_words = len(embeddings.vocab_list)
        self.embeddings.save_ann()
        if self.compute_parallel:
            mat = np.zeros((num_words, num_words))
            num_workers = mp.cpu_count()
            pool = mp.Pool(num_workers)
            num_samples_worker = self.num_samples // num_workers
            for i in np.arange(0, num_workers):
                result = pool.apply_async(self.get_transition_matrix_partial, args=(num_samples_worker, i))
                result = result.get()
                mat += result
            pool.close()
            pool.join()
        else:
            mat = self.get_transition_matrix_partial(self.num_samples)
        mat /= (num_samples_worker * num_workers)
        self.transition_matrix = mat
        return mat

    def get_transition_matrix_partial(self, num_samples, seed):
        np.random.seed(seed)
        self.embeddings.load_ann()
        embeddings = self.embeddings
        num_words = len(embeddings.vocab_list)
        mat = np.zeros((num_words, num_words))
        #parallel for loop
        for i in range(0, num_words):
            word = embeddings.vocab_list[i]
            for j in range(0, num_samples):
                w = self.get_private_word(word)
                mat[i][embeddings.vocab_dict[w]] += 1
        return mat

    
    """ Implementation of get_private_word for Euclidean Mechanism. Adds noise
        in embedding space and computes the nearest neighbor.
        Parameters:
            word: word to privatize
    """
    def get_private_word(self, word):
        embeddings = self.embeddings
        ann_index = embeddings.annoy_index
        word_vector = embeddings.embds_dict[word]
        dimension = word_vector.shape[0]
        noise = self.multivariate_laplace(dimension, self.epsilon)
        noisy_vector = word_vector + noise

        nearest_word_index = ann_index.get_nns_by_vector(noisy_vector, 1)
        nearest_word = embeddings.vocab_list[nearest_word_index[0]]

        return nearest_word

    @staticmethod
    def multivariate_laplace(dim: int, eps: float, sensitivity: float = 1.0):
        """
        :param dim: dimension
        :param eps: epsilon
        """
        N = np.random.normal(size=dim)
        X = N / np.linalg.norm(N)
        Y = np.random.gamma(dim, sensitivity / eps)
        Z = X * Y
        return Z

class ExponentialMechanism(MechanismBase):
    """ Creates an Exponential mechanism class. 
        Parameters:
            epsilon: privacy parameter used by this class.
            embeddings: word embeddings instance (see embeddings.py).
    """
    def __init__(self, epsilon, embeddings):
        super().__init__(epsilon, embeddings)
        self.name="Exponential"
    
    """ Computes exact transtion matrix for Exponential mechanism.
    """
    def get_transition_matrix(self):
        dist_mat = self.embeddings.distance_matrix
        embeddings = self.embeddings
        num_words = len(embeddings.vocab_list)
        half_eps = self.epsilon/2
        trans_mat = np.exp(-half_eps * dist_mat)
        row_sums = trans_mat.sum(axis=1)
        row_sums = row_sums.reshape((num_words, -1))
        trans_mat /= row_sums
        self.transition_matrix = trans_mat
        return trans_mat

    """ Gets a private word according to the Exponential mechanism.
        Samples from the transition matrix; therefore, this matrix must be
        computed first.
        Parameters:
            word: word to privatize.
    """ 
    def get_private_word(self, word):
        if self.transition_matrix is None:
            print('Please compute transition matrix')
        idx = self.embeddings.vocab_dict[word]
        return np.random.choice(self.embeddings.vocab_list, p =
        self.transition_matrix[idx])

class OPTMech(MechanismBase):
    """ Creates a Linear Program mechanism class.
        Parameters:
            epsilon: privacy parameter used by this class.
            embeddings: word embeddings instance (see embeddings.py).
    """
    def __init__(self, epsilon, embeddings, spanner=None, max_memory = None,
            max_exp = np.inf):
        super().__init__(epsilon, embeddings)
        self.name="LP"
        self.max_exp = max_exp
        if max_memory is None:
            self.max_lpsize = np.inf
        else:
            self.max_lpsize = max_memory
        if spanner is None:
            self.constr_matrix = np.ones_like(self.embeddings.distance_matrix,
                    dtype=int)
        else:
            self.constr_matrix = spanner

    
    """ Sets up the linear program whose optimal value is the
        mechanism that minimizes the loss out of all mechanisms which satisfy
        epsilon-metric DP.
        Parameters:
            D: metric used in definition of metric DP. This a matrix whose i,j
            element is the distance between word i and word j.
            epsilon: value of epsilon.
    """
    def setupLP(self, D, epsilon):
        n = D.shape[0]
        D_exp = np.minimum(D, self.max_exp / epsilon)
        C = lil_matrix((n**3+n, n**2+1))
        K_ind = n**2
        count = 0
        for i in range(0, n):
            for j in range(i+1, n):
                for k in range(0,n):
                    if self.constr_matrix[i][j] == 0:
                        continue
                     #x_{ik} \geq e^{-epsilon d_{ij}} x_{jk}
                    ik = i*n + k
                    jk = j*n + k
                    d = D_exp[i,j]
                    C[count,ik] = -1
                    C[count,jk] = np.exp(-epsilon * d)
                    count += 1
                    C[count,ik] = np.exp(-epsilon * d)
                    C[count,jk] = -1
                    count += 1
        for i in range(0, n**2):
            C[count, i] = -1
            count += 1
        for i in range(0, n):
            for j in range(0, n):
                C[count, j + i*n] = D[i, j]
            C[count, K_ind] = -1
            count += 1
        self.lp_shape = (C.count_nonzero(), (count, C.shape[1]))
        self.memory = (count + n) * C.shape[1]
        b = np.zeros(n**3+n)
        P = lil_matrix((n, n**2+1))
        for i in range(0, n):
            for j in range(0,n):
                P[i,i*n+j] = 1
        p = np.ones(n)
        obj = np.zeros(n**2+1)
        obj[K_ind] = 1
        return (obj, C, b, P, p)

    """ Computes exact transtion matrix for Exponential mechanism.
    """
    def get_transition_matrix(self):
        K = self.setupLP(self.embeddings.distance_matrix, self.epsilon)
        if K[1].count_nonzero() > self.max_lpsize:
            print("LP too big; aborting")
            self.transition_matrix = None
            return None
        L = linprog(*K, method='highs-ipm', options = {'disp': False})
        if not L['success']:
            print("LP not successful!")
            self.transition_matrix = None
            return None
        n = self.embeddings.distance_matrix.shape[0]
        trans_mat = L['x'][:-1].reshape((n,n))
        self.transition_matrix = trans_mat
        return trans_mat

    """ Gets a private word according to the Exponential mechanism.
        Samples from the transition matrix; therefore, this matrix must be
        computed first.
        Parameters:
            word: word to privatize.
    """ 
    def get_private_word(self, word):
        if self.transition_matrix is None:
            print('Please compute transition matrix')
        idx = self.embeddings.vocab_dict[word]
        return np.random.choice(self.embeddings.vocab_list, p =
        self.transition_matrix[idx])

class ConstrOPTMech(MechanismBase):

    """ Creates a Heuristic mechanism class. This class is identical to the LP
        mechanism class, except it fixes some of the P_{ij} variables to be
        fixed at Y_{j} \exp(-D_{ij} \epsilon), where Y_j are new variables.
        The class decides when do do this using its parameter I, which is an n
        by n matrix such that I[i,j] = 0 if it is to be fixed, and 1 otherwise.
        Parameters:
            epsilon: privacy parameter used by this class.
            embeddings: word embeddings instance (see embeddings.py).
            lambdas: amount of slack to use for constraint that transition matrix
                rows sum to 1. If a list, then the best choice of lambda will
                be used.
    """
    def __init__(self, epsilon, embeddings, lambdas, max_exp = np.inf,
            max_memory = None, spanner_coef = None):
        super().__init__(epsilon, embeddings)
        self.name="Heuristic"
        self.I = np.ones_like(embeddings.distance_matrix, dtype='int')
        self.lambdas = lambdas
        self.solver_method = 'highs-ipm'
        self.solver_options = {'disp': False}
        self.max_exp = max_exp
        if max_memory is None:
            self.max_lpsize = np.inf
        else:
            self.max_lpsize = max_memory

        self.spanner_coef = spanner_coef
    
    """ Sets the variables to not be fixed (self.I) to be those P_{ij} where i is
        not one of the r nearest neighbors to j.
        Paramters:
        num_nns: number of nearest neighbors to use (variables which are
            not one of the num_nns nearest points are fixed).
    """
    def set_nns_heuristic(self, nns):
        I = np.zeros_like(self.embeddings.distance_matrix, dtype=int)
        for i in range(0, I.shape[0]):
            closest_nbrs = \
                np.argsort(self.embeddings.distance_matrix[i,:])[:nns]
            I[i, closest_nbrs] = 1
        self.I = I

    """ Sets the variables to not be fixed (self.I) to be those P_{ij} where i is 
        one of r-1 randomly chosen words.
    """
    def set_random_heuristic(self, npts):
        n = self.embeddings.distance_matrix.shape[0]
        I = np.zeros_like(self.embeddings.distance_matrix, dtype=int)
        for i in range(0, I.shape[0]):
            random_nbrs = np.random.choice(n, npts, replace=False)
            I[random_nbrs, i] = 1
        self.I = I

    """ Gets the best transition matrix out of all lambdas to try set in
    self.lambdas
    """

    def get_transition_matrix(self):
        D = self.embeddings.distance_matrix
        min_loss = D.mean(axis=1).max()
        best_matrix = None
        for l in self.lambdas:
            T = self.get_transition_matrix_once(l)
            if T is None:
                continue
            loss = (T * D).sum(axis=1).max()
            if loss < min_loss:
                best_matrix = T
                min_loss = loss
                self.best_lambda = l
        self.transition_matrix = best_matrix
        return best_matrix

    """ Gets the transition matrix for a specific value of lambda
        This solves the heuristic L.P. and returns None if the solver fails.
    """
    def get_transition_matrix_once(self, lamb, normalize=True):
        D = self.embeddings.distance_matrix
        I = self.I
        epsilon = self.epsilon/2
        D_exp = np.minimum(self.max_exp / epsilon, D)
        n = D.shape[0]
        idxs = np.zeros((n,n), dtype=int)
        count = n
        for i in range(0, n):
            for j in range(0, n):
                if I[j,i] != 0:
                    idxs[j,i] = count
                    count += 1

        num_vars = count+1
        C = lil_matrix((n ** 3 + 3*n + 1, num_vars))
        count = 0
        for i in range(0, n):
            active_vars = np.where(I[:,i].flatten() != 0)[0]
            if self.spanner_coef is None:
                include = np.ones((n, n), dtype=int)
            else:
                point_dists = D[active_vars][:, active_vars]
                include, _ = get_spanner(point_dists, self.spanner_coef)
            for a in range(0, len(active_vars)):
                for b in range(0, len(active_vars)):
                    if a != b and include[a,b]:
                        j = active_vars[a]
                        k = active_vars[b]
                        j_idx = idxs[j,i]
                        k_idx = idxs[k,i]
                        row = np.zeros(num_vars)
                        #P_{ji} \leq \exp(D_{jk} * eps) P_{ki}
                        C[count, j_idx] = 1
                        C[count, k_idx] = -np.exp(D_exp[k, j] * epsilon)
                        count += 1

            non_active_vars = I[:,i] == 0
            if np.any(non_active_vars):
                for j in active_vars:
                    j_idx = idxs[j, i]
                    #P_{ji} \leq min over k: \exp(D_{jk} * eps) \exp(-D_{ki} * eps)
                    #                 * A_i
                    #P_{ji} \geq max over k: \exp(-D_{jk} * eps) \exp(-D_{ki} * eps)
                    #                 * A_i
                    coeff_up = (D_exp[j, non_active_vars] - D_exp[i, non_active_vars]).min()
                    coeff_down = (D_exp[j, non_active_vars] + D_exp[i, \
                        non_active_vars]).min() 
                    coeff_down = -coeff_down
                    C[count, j_idx] = 1
                    C[count, i] = -np.exp(coeff_up * epsilon)
                    count += 1
                    C[count, j_idx] = -1
                    C[count, i] = np.exp(coeff_down * epsilon)
                    count += 1

        for i in range(0, n):
            non_active_vars = np.where(I[i,:] == 0)[0]
            non_active_coefs = (lamb+D[i, non_active_vars]) * np.exp(-D_exp[i,non_active_vars] * epsilon)
            C[count, non_active_vars] = non_active_coefs

            active_vars = np.where(I[i,:] != 0)[0]
            active_vars_idxs = idxs[i, active_vars]
            C[count, active_vars_idxs] = (lamb+D[i, active_vars])
            C[count, -1] = -1
            count += 1

        for i in range(0, n):
            non_active_vars = np.where(I[i,:] == 0)
            non_active_coefs = np.exp(-D_exp[i,non_active_vars] * epsilon)
            C[count, non_active_vars] = -non_active_coefs

            active_vars = np.where(I[i,:] != 0)[0]
            active_vars_idxs = idxs[i, active_vars]
            C[count, active_vars_idxs] = -1
            count += 1
         
        C.resize((count, num_vars))
        self.memory = count * num_vars
        if C.count_nonzero() > self.max_lpsize:
            print("LP too large. Aborting")
            return None
        self.lp_shape = (C.count_nonzero(), (count, num_vars))
        b = np.zeros(count)
        b[count-n:count] = -1
        obj = np.zeros(num_vars)
        obj[-1] = 1
        
        L = linprog(obj, C, b, method=self.solver_method, \
            bounds = (0, None), options = self.solver_options)
        if not L['success']:
            print("LP not successful!")
            print("Message from Solver: %s" % L['message'])
            if L['x'] is None:
                return None
        sol = L['x']
        ans = np.empty((n,n))
        row_coefs = sol[0:n]
        for i in range(0, n):
            active_vars = np.where(I[i,:] != 0)[0]
            ans[i, active_vars] = sol[idxs[i, active_vars]]
            non_active_vars = I[i,:] == 0
            row = row_coefs * np.exp(-D_exp[i, :] * epsilon)
            ans[i, non_active_vars] = row[non_active_vars]

        if normalize:
            row_sums = ans.sum(axis=1)
            row_sums = row_sums.reshape((row_sums.size, -1))
            ans /= row_sums
        return ans

