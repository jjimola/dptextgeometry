import sys
import numpy as np
from embeddings import load_embeddings
from mechanism_base import OPTMech, ExponentialMechanism, EuclideanMechanism, ConstrOPTMech
from tqdm import tqdm
import time
import pickle
from run_experiments import get_data_once

emb = load_embeddings('data/fasttext_sample_1000_0.5.txt', 200)
emb.save_ann()

def test_r(emb, eps_list, r_list, l):
    ans = {}
    for r in r_list:
        print(r)
        constr_opt_mech = ConstrOPTMech(1.0, emb, [l], max_exp=10)
        constr_opt_mech.set_nns_heuristic(r)
        ans[r] = []
        for e in eps_list:
            constr_opt_mech.epsilon = e
            res = get_data_once(constr_opt_mech)
            if res['success']:
                ans[r].append(res)
    return ans

def test_l(emb, eps_list, r, l_list):
    ans = {}
    for l in l_list:
        print(l)
        constr_opt_mech = ConstrOPTMech(1.0, emb, [l], max_exp=10)
        constr_opt_mech.set_nns_heuristic(r)
        ans[l] = []
        for e in eps_list:
            constr_opt_mech.epsilon = e
            res = get_data_once(constr_opt_mech)
            if res['success']:
                ans[l].append(res)
    return ans

"""
eps_list = np.arange(0.5, 2.0, 0.2)
r_results = test_r(emb, eps_list, range(0, 10), 0.1)
pickle.dump(r_results, open('r_param.pkl', 'wb'))
l_results = test_l(emb, eps_list, 5, {0.001, 0.01, 0.1, 1.0, 10.0})
pickle.dump(l_results, open('l_param.pkl', 'wb'))
"""

l_data = pickle.load(open('l_param.pkl', 'rb'))
r_data = pickle.load(open('r_param.pkl', 'rb'))

test_eps = 1.0

r_results = []
for r_val, fixed_r in r_data.items():
    utis = [np.quantile(trial['utility'], 0.95) for trial in fixed_r]
    epss = [trial['epsilon'] for trial in fixed_r]
    uti = np.interp(test_eps, epss, utis)
    size = fixed_r[0]['lp_shape'][0]
    time = fixed_r[0]['time']
    r_results.append((r_val, uti, size, time))
    
    if r_val % 2:
        print('%2d%7.2f%7d%7.2f' % (r_val, uti, size, time))

l_results = []
l_keys = list(l_data.keys())
l_keys.sort()
for l_val in l_keys:
    fixed_l = l_data[l_val]
    utis = [np.quantile(trial['utility'], 0.95) for trial in fixed_l]
    epss = [trial['epsilon'] for trial in fixed_l]
    uti = np.interp(test_eps, epss, utis)
    size = fixed_l[0]['lp_shape'][0]
    time = fixed_l[0]['time']

    print('%7.3f%7.2f%7d%7.2f' % (l_val, uti, size, time))
