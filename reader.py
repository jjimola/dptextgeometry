import json
import os
from collections import defaultdict
from typing import Any, DefaultDict, Dict, List, Tuple


def _process_embeddings(f, limit=None):
    count = 0
    pairs = []
    dim = None

    for line in f:
        fields = line.strip().split(" ")
        if len(fields) == 2:
            continue

        if not dim:
            dim = len(fields) - 1

        if len(fields) == dim + 1:
            word = fields[0]
            vec = [float(v) for v in fields[1:]]
            pairs.append((word, vec))
            count += 1
        if count == limit:
            return pairs

    return pairs


def load_embeddings_as_dict(path, limit=None):
    """
    Load a limited number of embeddings from the given file path
    """
    with open(path, encoding="UTF-8") as f:
        pairs = _process_embeddings(f, limit)

    return dict(pairs)


def load_embeddings_from_bytes_as_dict(embeddings_bytes, limit=None):
    embeddings_array = [x.decode() for x in embeddings_bytes.split(b"\n")]

    return dict(_process_embeddings(embeddings_array, limit))


def load_words_as_dict(path, stripchars=None, defaultvalue=1):
    words_dict = {}

    with open(path) as f:
        for line in f:
            word = line.strip()
            if stripchars:
                for s in stripchars:
                    word = word.replace(s, "")

            words_dict[word] = defaultvalue

    return words_dict


def load_poincare_noise(dir_path: str) -> DefaultDict[Any, Any]:
    """
    Load generated hyperbolic noise samples from disk. This is required
    because we cannot directly sample from the desired hyperbolic space
    """
    noise_samples = defaultdict(list)  # type: DefaultDict[float, list]

    for noise_file in os.listdir(dir_path):
        noise_path = os.path.join(dir_path, noise_file)
        assert noise_file.endswith(".json"), "Unable to load non-json noise file"

        epsilon = float(noise_file.replace(".json", ""))
        with open(noise_path) as f:
            noise_samples[epsilon] = json.load(f)

    return noise_samples
