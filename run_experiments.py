import numpy as np
from embeddings import load_embeddings
from mechanism_base import OPTMech, ExponentialMechanism, EuclideanMechanism, ConstrOPTMech
from tqdm import tqdm
from lower_bound import lower_bound
import pickle
import multiprocessing as mp
import time

"""
    Gets privacy, utility (loss), transition matrix, N_w, S_w, time, and memory
    (for certain mechanisms) data for a particular mechanism. Returns this data in a
    dictionary. If the mechaism is not successful, returns a dictionary with a
    success flag set to False

    Parameters:
    mech: Mechanism class for which to collect data.
"""
def get_data_once(mech):
    mech.embeddings.load_ann()
    start = time.time()
    _ = mech.get_transition_matrix()
    dur = time.time() - start
    if mech.transition_matrix is None:
        D = {"success": False}
        if hasattr(mech, 'memory'):
            D['memory'] = mech.memory
        if hasattr(mech, 'lp_shape'):
            D['lp_shape'] = mech.lp_shape
        return D

    loss = mech.get_loss('whole')
    #1e-4 is used as delta, but I compute it with delta=0 in the Jupyter
    #notebook
    eps_actual = mech.get_actual_epsilon(0.01)
    t_mats = mech.transition_matrix
    success = True 
    nw = mech.get_Nw_vec() 
    sw = mech.get_Sw_vec(0.01)
    D = {"utility": loss, "epsilon": eps_actual, "trans": t_mats, "nw": nw, "sw": sw,
            "time": dur, "success": success}

    if hasattr(mech, 'memory'):
        D['memory'] = mech.memory
    if hasattr(mech, 'lp_shape'):
        D['lp_shape'] = mech.lp_shape
    return D

"""
    Gets data for many mechanisms in a list in parallel. If the memory
    requirments of the mechanism computations are high, then this should not
    be used.

    Parameters:
    M_list: lists of Mechanism classes for which to collect data.
    verbose: print a line indicating success for each mechanims in the list.
"""
def get_data_parallel(M_list, verbose):
    num_workers = mp.cpu_count()

    pool = mp.Pool(num_workers)
    res = []
    for task in M_list:
        result = pool.apply_async(get_data_once, args = (task,))
        res.append( result.get() )
        if verbose:
            print("epsilon %0.2f: Success: %s" % (task.epsilon, str(result['success'])))
    pool.close()
    pool.join()

    return res

"""
    Gets data for many mechanisms in a list sequentially. Uses a progress bar
    to show progress.

    Parameters:
    M_list: lists of Mechanism classes for which to collect data.
    verbose: print a line indicating success for each mechanims in the list.
"""
def get_data_sequential(M_list, verbose):
    res = []
    for task in tqdm(M_list):
        result = get_data_once(task)
        res.append(result)
        if verbose:
            print("epsilon %0.2f: Success: %s" % (task.epsilon, str(result['success'])))

    return res

"""
    Runs experiments for a particular type of mechanism and particular
    embedding of a fixed size, on a range of epsilon values.
    Parameters:

    directory: embedding directory
    num_pts: number of points to load from embedding
    epsilons: list of epsilons to test on
    mechanism: string describing which mechanism to test---either euclidean,
        exponential, lp, or heuristic.
    parallel: whether to run the different epsilon values in parallel or not.
    verbose: whether to print messages indicating success
    save_directory: directory to save the results in. If None, then this action
        is not done.
    suffix: suffix to add to saved file name to avoid name clashes.
"""
def run_experiments(directory, num_pts, epsilons, mechanism, 
        mechanism_args = {}, parallel=False, verbose=False,
        save_directory=None, suffix=''):
    e2 = load_embeddings(directory, num_pts)
    e2.save_ann()
    
    mechs = []
    if mechanism == 'euclidean':
        mechs = [EuclideanMechanism(eps, mechanism_args['num_samples'], e2, compute_parallel=mechanism_args['parallel']) \
                for eps in epsilons]
    elif mechanism == 'exponential':
        mechs = [ExponentialMechanism(eps, e2) for eps in epsilons]
    elif mechanism == 'lp':
        mechs = [OPTMech(eps, e2) for eps in epsilons]
    elif mechanism == 'heuristic':
        for eps in epsilons:
            h_mech = ConstrOPTMech(eps, e2, mechanism_args['lambdas'], max_exp = 11)
            h_mech.set_nns_heuristic(mechanism_args['num_nn'])
            if 'solver' in mechanism_args:
                h_mech.solver_method=mechanism_args['solver']
            mechs.append(h_mech)
    else:
        print('Mechanism "%s" not recognized' % mechanism)
        return None

    if parallel:
        res = get_data_parallel(mechs, verbose)
    else:
        res = get_data_sequential(mechs, verbose)

    ans = {}
    for d in res:
        if d['success']:
            for k,v in d.items():
                try:
                    ans[k].append(v)
                except:
                    ans[k] = [v]
        else:
            ans['success'].append(False)

    if save_directory is not None:
        pickle.dump(ans, open(save_directory + mechanism + suffix+ '.pkl', 'wb'))

    return ans

"""
    Collects the loss vs epsilon data on the 60 word vocabulary
    Parameters:
    epsilons: dictionary containing range of epsilons to test.
    emb_file: embedding file to use
    save_dir: directory to save results to
"""
def run_small(epsilons, emb_file, save_dir):

    num_pts = 60
    heuristic_args = {'solver': 'highs-ipm', 'lambdas': [0.0, 0.1, 1.0, 10.0, 100.0], 'num_nn': 10}
    _ = run_experiments(emb_file, num_pts, epsilons['heuristic'],
    'heuristic', mechanism_args = heuristic_args, save_directory=save_dir)
    _ = run_experiments(emb_file, num_pts, epsilons['exponential'],
    'exponential', save_directory=save_dir)
    euclidean_args = {'num_samples': 20000, 'parallel': True}
    _ = run_experiments(emb_file, num_pts, epsilons['euclidean'], 
            'euclidean', mechanism_args=euclidean_args, parallel=False, save_directory=save_dir)
    _ = run_experiments(emb_file, num_pts, epsilons['lp'], 
            'lp', save_directory=save_dir)


"""
    Collects the loss vs epsilon data on the 200 word vocabulary
    Parameters:
    epsilons: dictionary of range of epsilons to test for each mechanism.
    emb_file: embedding file to use
    save_dir: directory to save results to
"""
def run_large(epsilons, emb_file, save_dir):

    num_pts = 200
    heuristic_args = {'solver': 'highs-ipm', 'lambdas': [0.0, 0.1, 1.0, 10.0, 100.0], 'num_nn': 10}
    #The LP does not work for the higher values of epsilon --- numbers become too large
    _ = run_experiments(emb_file, num_pts, epsilons['heuristic'],
    'heuristic', mechanism_args = heuristic_args, save_directory=save_dir)
    _ = run_experiments(emb_file, num_pts, epsilons['exponential'],
    'exponential', save_directory=save_dir)
    euclidean_args = {'num_samples': 200000, 'parallel': True}
    _ = run_experiments(emb_file, num_pts, epsilons['euclidean'],
    'euclidean', mechanism_args=euclidean_args, parallel=False, save_directory=save_dir)

"""
    Runs experiments for a particular type of mechanism and particular
    embedding for a fixed epsion, for a range of embedding sizes.
    Parameters:

    directory: embedding directory
    num_pts: list of number of points to test.
    epsilons: epsilon to use for experiment.
    mechanism: string describing which mechanism to test---either
        lp or heuristic
    parallel: whether to run the different epsilon values in parallel or not.
    verbose: whether to print messages indicating success
    save_directory: directory to save the results in. If None, then this action
        is not done.
    suffix: suffix to add to saved file name to avoid name clashes.
"""

def run_experiments_timing(directory, num_pts_list, epsilon, mechanism, 
        mechanism_args = {}, parallel=False, verbose=False, suffix='',
        save_directory=None):
    
    mechs = []
    for num_pts in num_pts_list:
        e2 = load_embeddings(directory, num_pts)
        if mechanism == 'lp':
            mechs.append(OPTMech(epsilon, e2))
        elif mechanism == 'heuristic':
            h_mech = ConstrOPTMech(epsilon, e2, mechanism_args['lambdas'])
            h_mech.set_nns_heuristic(mechanism_args['num_nn'])
            if 'solver' in mechanism_args:
                h_mech.solver_method=mechanism_args['solver']
            mechs.append(h_mech)
        else:
            print('Mechanism "%s" not recognized' % mechanism)
            return None

    if parallel:
        res = get_data_parallel(mechs, verbose)
    else:
        res = get_data_sequential(mechs, verbose)

    ans = {}
    for d in res:
        if d['success']:
            for k,v in d.items():
                try:
                    ans[k].append(v)
                except:
                    ans[k] = [v]
        else:
            ans['success'].append(False)

    ans['num_pts'] = num_pts_list
    ans['memory'] = [d['memory'] for d in res]
    if save_directory is not None:
        pickle.dump(ans, open(save_directory + mechanism + suffix + '.pkl', 'wb'))

    return ans

    """
    Collects the timing data used in our paper
    Parameters:
    emb_file: embedding file to use
    save_dir: directory to save results to
    """
def run_timing(emb_file, save_dir):
    num_pts = [50, 100, 200, 300, 500, 1000]
    
    heuristic_args = {'solver': 'highs-ipm', 'lambdas': [0.0], 'num_nn': 5}
    run_experiments_timing(emb_file, num_pts, 1.0,
    'heuristic', mechanism_args = heuristic_args, suffix='_5', save_directory=save_dir)
    heuristic_args = {'solver': 'highs-ipm', 'lambdas': [0.0], 'num_nn': 10}
    run_experiments_timing(emb_file, num_pts, 1.0,
    'heuristic', mechanism_args = heuristic_args, suffix='_10', save_directory=save_dir)
    heuristic_args = {'solver': 'highs-ipm', 'lambdas': [0.0], 'num_nn': 20}
    run_experiments_timing(emb_file, num_pts[:5], 1.0,
    'heuristic', mechanism_args = heuristic_args, suffix='_20', save_directory=save_dir)
    run_experiments_timing(emb_file, num_pts[:2], 1.0,
            'lp', suffix='_small', save_directory=save_dir)

def get_actual_epsilon(T, D, delta):
    n = T.shape[1]
    eps_actuals = []
    for i in range(0, n):
        eps_actual = 0
        for j in range(0, n):
            if i == j:
                continue
            d = D[i,j]
            P = T[i,:]
            Q = T[j,:]
            support = np.any((P > 0, Q > 0), axis=0)
            P = P[support]
            Q = Q[support]
            #Division by 0 has the proper behavior here.
            with np.errstate(divide='ignore'):
                ratios = P / Q
            idxs = np.argsort(ratios)
            probs = Q[idxs].cumsum()
            #The last elements of probs must be 1.
            probs[probs == probs[-1]] = 1
            #cutoff is the smallest index where >= 1-delta of Q's mass is
            cutoff = np.searchsorted(probs, 1-delta, side='left')
            eps = np.log(ratios[idxs[cutoff]]) / d
            eps_actual = max(eps_actual, eps)
        eps_actuals.append(eps_actual)
    return np.array(eps_actuals)


"""
#These are the experiments I use for the paper
#Warning: This takes like 20 hours to run.
epsilons_gs = np.arange(0.2, 4.0, 0.3)
epsilons_glove_small = {'euclidean': epsilons_gs * 4, 'heuristic': epsilons_gs, 
                        'lp': epsilons_gs, 'exponential': epsilons_gs}
run_small(epsilons_glove_small, 'data/glove_sample_1000_0.5.txt','data/glove/60-pts-results/')
epsilons_gl = np.arange(4.0, 0, -0.3)
epsilons_glove_large = {'euclidean': epsilons_gl * 4, 'heuristic': epsilons_gl,
                        'exponential': epsilons_gl}
run_large(epsilons_glove_large, 'data/glove_sample_1000_0.5.txt','data/glove/200-pts-results/')
run_timing('data/glove_sample_1000_0.5.txt','data/glove/varying_num_pts/')
epsilons_fs = np.arange(0.2, 4.0, 0.3)
epsilons_fastt_small = {'euclidean': epsilons_fs * 16, 'heuristic': epsilons_fs * 2, 
                        'lp': epsilons_fs, 'exponential': epsilons_fs * 2}
run_small(epsilons_fastt_small, 'data/fasttext_sample_1000_0.5.txt','data/fasttext/60-pts-results/')
epsilons_fastt_large = {'euclidean': epsilons_fs * 16, 'heuristic': epsilons_fs * 3,
                        'exponential': epsilons_fs * 3}
run_large(epsilons_fastt_large, 'data/fasttext_sample_1000_0.5.txt','data/fasttext/200-pts-results/')
run_timing('data/fasttext_sample_1000_0.5.txt','data/fasttext/varying_num_pts/')
"""
