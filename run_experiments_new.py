import sys
import numpy as np
from embeddings import load_embeddings
from mechanism_base import OPTMech, ExponentialMechanism, EuclideanMechanism, ConstrOPTMech
from constr_mech import ExpConstrOPTMech
from tqdm import tqdm
import time
import pickle

from run_experiments import get_data_once

def get_data(mech, epsilons, save_path = None, stop_after = np.inf):
    res = []
    for e in epsilons:
        print(e)
        sys.stdout.flush()
        mech.epsilon = e
        r = get_data_once(mech)
        res.append(r)
        if r['success'] and r['epsilon'] > stop_after:
            break

    res_dict = {}
    for i in range(0, len(res)):
        for k, v in res[i].items():
            if k in res_dict:
                res_dict[k].append(v)
            else:
                res_dict[k] = [v]
    res_dict['name'] = mech.name
    res_dict['distance_matrix'] = mech.embeddings.distance_matrix
    if save_path is not None:
        pickle.dump(res_dict, open(save_path, 'wb'))
    return res_dict

print('Starting')
sys.stdout.flush()

euclid_eps_fastt = np.geomspace(1, 70, 10)
exp_eps_fastt = np.geomspace(0.1, 12.0, 10)
span_eps_fastt = np.geomspace(0.1, 5.0, 10)
constr5_eps_fastt = np.geomspace(0.1, 12.0, 10)
constr10_eps_fastt = np.geomspace(0.1, 12.0, 10)

euclid_eps_glove = np.geomspace(1, 35, 10)
exp_eps_glove = np.geomspace(0.05, 3.5, 10)
span_eps_glove = np.geomspace(0.05, 1.4, 10)
constr5_eps_glove = np.geomspace(0.05, 3.5, 10)
constr10_eps_glove = np.geomspace(0.05, 3.5, 10)

euclid_eps_geo = np.geomspace(0.1, 0.5, 10)
exp_eps_geo = np.concatenate((np.geomspace(0.02, 0.13, 5), 
    np.linspace(0.2, 2.5,5)))
span_eps_geo = np.concatenate((np.geomspace(0.005, 0.1, 5), 
    np.linspace(0.15, 0.65, 5)))
constr5_eps_geo = np.concatenate((np.geomspace(0.02, 0.13, 5), 
    np.linspace(0.2, 2.5, 5)))
constr10_eps_geo = np.concatenate((np.geomspace(0.02, 0.13, 5), 
    np.linspace(0.2, 2.5, 5)))

#200pts

def do_200(prefix, emb, euclid_eps, exp_eps, span_eps, constr5_eps, constr10_eps):
    euclid_mech = EuclideanMechanism(1.0, 200000, emb, True)
    euclid_data = get_data(euclid_mech, euclid_eps,
            prefix+'euclid_data.pkl')
    exp_mech = ExponentialMechanism(1.0, emb)
    exp_data = get_data(exp_mech, exp_eps, prefix+'exp_data.pkl')
    spanner, _ = emb.get_spanner(2.0)
    spanner_opt_mech = OPTMech(1.0, emb, spanner, max_exp=10)
    span_data = get_data(spanner_opt_mech, span_eps, prefix+'spanner_data.pkl')
    constr_opt_mech = ConstrOPTMech(1.0, emb, [0.001, 0.1, 1.0], max_exp=10)
    constr_opt_mech.set_nns_heuristic(5)
    constr_data = get_data(constr_opt_mech, constr5_eps,
        prefix+'constr_data_5.pkl')
    constr_opt_mech.set_nns_heuristic(10)
    constr_data = get_data(constr_opt_mech, constr10_eps,
        prefix+'constr_data_10.pkl')

emb = load_embeddings('data/fasttext_sample_1000_0.5.txt', 200)
emb.save_ann()
do_200('data/200pts/fasttext/', emb, euclid_eps_fastt, exp_eps_fastt,
        span_eps_fastt, constr5_eps_fastt, constr10_eps_fastt)

emb = load_embeddings('data/glove_sample_1000_0.5.txt', 200)
emb.save_ann()
do_200('data/200pts/glove/', emb, euclid_eps_glove, exp_eps_glove, span_eps_glove,
        constr5_eps_glove, constr10_eps_glove)

emb = load_embeddings('data/geolife.txt', 200)
emb.save_ann()
do_200('data/200pts/geo/', emb, euclid_eps_geo, exp_eps_geo, span_eps_geo,
        constr5_eps_geo, constr10_eps_geo)

def do_varying_points(mech, emb_path, pts_list, epsilon_list, save_path = None,
        nns = None, stop_after = np.inf):
    res = {}
    for pts in pts_list:
        print(pts)
        emb = load_embeddings(emb_path, pts)
        emb.save_ann()
        mech.embeddings = emb
        if hasattr(mech, 'constr_matrix'):
            spanner, _ = emb.get_spanner(2)
            mech.constr_matrix = spanner
        if nns is not None:
            mech.set_nns_heuristic(nns)
        res_dict = get_data(mech, epsilon_list, stop_after = stop_after)
        res[pts] = res_dict
        
    if save_path is not None:
        pickle.dump(res, open(save_path, 'wb'))
    return res

def do_vary(prefix, emb_path, emb_dummy, pts, max_memory, exp_eps, 
        span_eps, constr5_eps, constr10_eps, stop_after = np.inf):
    print('Exponential')
    #exp_eps = exp_eps[exp_eps >= stop_after]
    exp_mech = ExponentialMechanism(1.0, emb_dummy)
    _ = do_varying_points(exp_mech, emb_path, pts,
            exp_eps, prefix+'exp_data.pkl', stop_after = stop_after)
    print('Constr 10')
    #constr10_eps = constr10_eps[constr10_eps >= stop_after]
    constr_opt_mech = ConstrOPTMech(1.0, emb_dummy, [0.001, 0.1, 1.0],
            max_memory = max_memory, max_exp=10)
    _ = do_varying_points(constr_opt_mech, emb_path, pts,
            constr10_eps, prefix+'constr_data_10.pkl', 10,
            stop_after = stop_after)
    print('Constr 5')
    constr_opt_mech = ConstrOPTMech(1.0, emb_dummy, [0.001, 0.1, 1.0],
            max_memory = max_memory, max_exp=10)
    #constr5_eps = constr5_eps[constr5_eps >= stop_after]
    _ = do_varying_points(constr_opt_mech, emb_path, pts,
            constr5_eps, prefix+'constr_data_5.pkl', 5, stop_after = stop_after)
    print('Spanner')
    spanner, _ = emb.get_spanner(2.0)
    spanner_opt_mech = OPTMech(1.0, emb_dummy, spanner, max_memory = max_memory,
            max_exp=10)
    _ = do_varying_points(spanner_opt_mech, emb_path, pts,
            span_eps, prefix+'spanner_data_300.pkl', stop_after = stop_after)

pts = np.arange(50, 450, 50)
max_memory = 2e6
emb = load_embeddings('data/glove_sample_1000_0.5.txt', 200)

prefix = 'data/vary_pts/glove/'
emb_path = 'data/glove_sample_1000_0.5.txt'
do_vary(prefix, emb_path, emb, pts, max_memory,
        np.arange(0.8, 1.2, 0.1) * 2,
        np.arange(0.4, 1.4, 0.2) * 0.67, # 1 is in 1.5 L and in 3 L
        np.arange(0.8, 1.2, 0.1) * 2, 
        np.arange(0.8, 1.2, 0.1) * 2)

prefix = 'data/vary_pts/fasttext/'
emb_path = 'data/fasttext_sample_1000_0.5.txt'
do_vary(prefix, emb_path, emb, pts, max_memory, 
        np.arange(1.6, 2.6, 0.2) * 2, 
        np.arange(0.8, 2.8, 0.4) * 0.67,
        np.arange(1.6, 2.6, 0.2) * 2, 
        np.arange(1.6, 2.6, 0.2) * 2)

prefix = 'data/vary_pts/geo/'
emb_path = 'data/geolife.txt'
do_vary(prefix, emb_path, emb, pts, max_memory, 
        np.arange(0.1, 0.4, 0.05), 
        np.arange(0.1, 0.4, 0.05)/3,
        np.arange(0.1, 0.4, 0.05),
        np.arange(0.1, 0.4, 0.05))

